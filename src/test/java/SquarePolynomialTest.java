import org.junit.Test;

import static org.junit.Assert.*;

public class SquarePolynomialTest {
    private static final double ACC = 0.00001;

    @Test
    public void testGetRootsNoRoots() throws Exception {
        SquarePolynomial sp = new SquarePolynomial(1, 1, 1);
        double[] expected = new double[0];
        assertArrayEquals(expected, sp.getRoots(), ACC);
    }

    @Test
    public void testGetRootsOneRoot() throws Exception {
        SquarePolynomial sp = new SquarePolynomial(1, -2, 1);
        double[] expected = new double[] { 1.0 };
        assertArrayEquals(expected, sp.getRoots(), ACC);
    }

    @Test
    public void testGetRootsTwoRoot() throws Exception {
        SquarePolynomial sp = new SquarePolynomial(1, -5, 6);
        double[] expected = new double[] { 2.0, 3.0 };
        assertArrayEquals(expected, sp.getRoots(), ACC);
    }

    @Test(expected = NoRootsException.class)
    public void testSplitNoRoots() throws Exception {
        SquarePolynomial sp = new SquarePolynomial(1, 1, 1);
        sp.split();
    }

    @Test
    public void testSplitWithOneRoot() throws Exception {
        SquarePolynomial sp = new SquarePolynomial(1, -2, 1);
        Polynomial[] splitting = new Polynomial[] {
                new Polynomial(1),
                new Polynomial(-1, 1),
                new Polynomial(-1, 1)
        };
        assertArrayEquals(splitting, sp.split());
    }

    @Test
    public void testSplitWithTwoRoots() throws Exception {
        SquarePolynomial sp = new SquarePolynomial(1, -5, 6);
        Polynomial[] splitting = new Polynomial[] {
                new Polynomial(1),
                new Polynomial(-2, 1),
                new Polynomial(-3, 1)
        };
        assertArrayEquals(splitting, sp.split());
    }
}