public class SquarePolynomial extends Polynomial {
    public SquarePolynomial(double a, double b, double c) throws BadCoeffsException {
        super(c, b, a);
        if (Double.compare(a, 0.0) == 0) {
            throw new BadCoeffsException("The highest coeff must be non zero");
        }
    }

    public SquarePolynomial(String str) throws BadCoeffsException {
        super(str);
        if (getDegree() != 2) {
            throw new BadCoeffsException("Bad polynomial string format");
        }
    }

    public double getA() {
        return getCoef(2);
    }

    public double getB() {
        return getCoef(1);
    }

    public double getC() {
        return getCoef(0);
    }

    public double getDiscr() {
        return getB() * getB() - 4 * getA() * getC();
    }

    public double[] getRoots() {
        double discr = getDiscr();
        if (discr < 0) {
            return new double[0];
        }
        if (discr > 0) {
            return new double[] {
                    (-getB() - Math.sqrt(discr)) / (2 * getA()),
                    (-getB() + Math.sqrt(discr)) / (2 * getA())
            };
        }
        return new double[] { -getB() / (2 * getA()) };
    }

    // a(x - x1)(x - x2)
    public Polynomial[] split() throws NoRootsException {
        double[] roots = getRoots();
        if (roots.length == 0) {
            throw new NoRootsException("Cannot split polynomial without roots");
        }
        if (roots.length == 1) {
            return new Polynomial[] {
                    new Polynomial(getA()),
                    new Polynomial(-roots[0], 1),
                    new Polynomial(-roots[0], 1)
            };
        }
        return new Polynomial[] {
                new Polynomial(getA()),
                new Polynomial(-roots[0], 1),
                new Polynomial(-roots[1], 1)
        };
    }
}
