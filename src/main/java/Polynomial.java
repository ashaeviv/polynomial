import java.util.Arrays;

public class Polynomial {
    private double[] coeffs;

    public Polynomial(double ... coeffs) {
        if (coeffs.length == 0) {
            this.coeffs = new double[] { 0.0 };
        } else {
            int nonZeroIndex = coeffs.length - 1;
            for (int i = coeffs.length - 1; i >= 0; i--) {
                if (Double.compare(coeffs[i], 0.0) != 0) {
                    nonZeroIndex = i;
                    break;
                }
            }
            this.coeffs = new double[nonZeroIndex + 1];
            for (int i = 0; i <= nonZeroIndex; i++) {
                this.coeffs[i] = coeffs[i];
            }
        }
    }

    public Polynomial(String str) {
        // TODO: implement it correctly
        this.coeffs = new double[] { 0.0 };
    }

    public double getCoef(int idx) { return coeffs[idx]; }

    public int getDegree() {
        return coeffs.length - 1;
    }

    public double getValue(double x) {
        double value = 0.0;
        double powX = 1.0;
        for (double coeff: coeffs) {
            value += (coeff * powX);
            powX *= x;
        }
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Polynomial)) return false;
        Polynomial that = (Polynomial) o;
        return Arrays.equals(coeffs, that.coeffs);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(coeffs);
    }

    @Override
    public String toString() {
        // an x^n + ... + a1 x^1 + a0 x^0
        StringBuilder sb = new StringBuilder();
        sb.append(coeffs[coeffs.length - 1])
                .append("x^")
                .append(coeffs.length - 1);
        for (int i = coeffs.length - 2; i >= 0; i--) {
            if (coeffs[i] > 0) {
                sb.append(" + ");
            } else if (coeffs[i] == 0) {
                continue;
            } else {
                sb.append(" - ");
            }
            sb.append(Math.abs(coeffs[i])).append("x^").append(i);
        }
        return sb.toString();
    }

    public Polynomial derive() {
        if (getDegree() == 0) {
            return new Polynomial();
        }
        double[] deriveCoeffs = new double[coeffs.length - 1];
        for (int i = 0; i < deriveCoeffs.length; i++) {
            deriveCoeffs[i] = (i + 1) * coeffs[i + 1];
        }
        return new Polynomial(deriveCoeffs);
    }
}
