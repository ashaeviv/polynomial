public class BadCoeffsException extends Exception {
    public BadCoeffsException(String message) {
        super(message);
    }

    public BadCoeffsException(String message, Throwable cause) {
        super(message, cause);
    }
}
