import org.junit.Test;

import static org.junit.Assert.*;

public class PolynomialTest {
    private static double ACC = 0.00001;

    @Test
    public void testCreateWithCoeffsCorrectly() {
        Polynomial p = new Polynomial(1, 2, 3);
        assertEquals(2, p.getDegree());
        assertEquals(1.0, p.getCoef(0), ACC);
        assertEquals(2.0, p.getCoef(1), ACC);
        assertEquals(3.0, p.getCoef(2), ACC);
    }

    @Test
    public void testCreateWithoutCoeffs() {
        Polynomial p = new Polynomial();
        assertEquals(0, p.getDegree());
        assertEquals(0.0, p.getCoef(0), ACC);
    }


    @Test
    public void testCreateWithLeadingZeroes() {
        Polynomial p = new Polynomial(1, 2, 0);
        assertEquals(1, p.getDegree());
        assertEquals(1.0, p.getCoef(0), ACC);
        assertEquals(2.0, p.getCoef(1), ACC);
    }

    @Test
    public void testGetDegreeWithSeveralCoeffs() {
        Polynomial p = new Polynomial(1, 2, 3);
        assertEquals(2, p.getDegree());
    }

    @Test
    public void testGetDegreeWithOneCoeff() {
        Polynomial p = new Polynomial(1);
        assertEquals(0, p.getDegree());
    }

    @Test
    public void testGetValueWithSeveralCoeffs() {
        Polynomial p = new Polynomial(1, 2, 3);
        assertEquals(17.0, p.getValue(2.0), ACC);
    }

    @Test
    public void testGetValueConstPolynom() {
        Polynomial p = new Polynomial(2);
        assertEquals(2.0, p.getValue(10.0), ACC);
    }

    @Test
    public void testToStringPositiveCoeffs() {
        Polynomial p = new Polynomial(1, 2, 3);
        String expected = "3.0x^2 + 2.0x^1 + 1.0x^0";
        assertEquals(expected, p.toString());
    }

    @Test
    public void testToStringWithNegativeCoeffs() {
        Polynomial p = new Polynomial(1, -2, 3);
        String expected = "3.0x^2 - 2.0x^1 + 1.0x^0";
        assertEquals(expected, p.toString());
    }

    @Test
    public void testToStringSkipZeroCoeffs() {
        Polynomial p = new Polynomial(1, 0, 3);
        String expected = "3.0x^2 + 1.0x^0";
        assertEquals(expected, p.toString());
    }

    @Test
    public void testToStringPositiveConstant() {
        Polynomial p = new Polynomial(1);
        String expected = "1.0x^0";
        assertEquals(expected, p.toString());
    }

    @Test
    public void testToStringNegativeConstant() {
        Polynomial p = new Polynomial(-1);
        String expected = "-1.0x^0";
        assertEquals(expected, p.toString());
    }

    @Test
    public void testToStringZeroConstant() {
        Polynomial p = new Polynomial(0);
        String expected = "0.0x^0";
        assertEquals(expected, p.toString());
    }

    @Test
    public void testDeriveNonConstPolynom() {
        Polynomial p = new Polynomial(1, 2, 3);
        Polynomial expected = new Polynomial(2, 6);
        assertEquals(expected, p.derive());
    }

    @Test
    public void testDeriveConstPolynom() {
        Polynomial p = new Polynomial(1);
        Polynomial expected = new Polynomial(0.0);
        assertEquals(expected, p.derive());
    }
}